# Deploy PGAdmin & WebApp with ansible role 

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# 1. Clonez le repo 

````bash
git clone https://gitlab.com/CarlinFongang-Labs/projet-odoo/deploy-odoo-postgresql-pgadmin/pgadmin-webapp-with-ansible.git
````

````bash
cd /path/odoo-postgresql-ansible
````

# 2. Ajustez les variables

````bash
nano group_vars/all.yml
````
Ajustez les variables selon le contexte :
[Indications sur les variables](https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/pgadmin_deploy#3-ajust-variables)


# 3. Importez les rôles

````bash
ansible-galaxy install -r roles/requirements.yml -f
````


# 4. Lancez la stack

## 4.1. Serveurs distants
````bash
ansible-playbook -i host.yml pgadmin-deploy -K -vvv
````


## 4.2. En local

````bash
ansible-playbook pgadmin-deploy -K -vvv
````



🥳🎉🥳 : Félicitation, PGAdmin et IC-WebApp sont déployé sur votre serveur


